﻿
$ErrorActionPreference = 'Stop';
$toolsDir   = "$(Split-Path -parent $MyInvocation.MyCommand.Definition)"
$fileLocation = Join-Path $toolsDir 'scrt_sfx83-x64.exe'
$url        = ''
$url64      = ''

$packageArgs = @{
  packageName   = $env:ChocolateyPackageName
  unzipLocation = $toolsDir
  fileType      = 'EXE'
  url           = $url
  url64bit      = $url64
  file         = $fileLocation

  softwareName  = 'securecrt*'

  checksum      = '59AC1C4AAB89C24A938BE8C16DE2A87A1A40CAAA425DC28F7F31185443A0AA61'
  checksumType  = 'sha256'
  checksum64    = '59AC1C4AAB89C24A938BE8C16DE2A87A1A40CAAA425DC28F7F31185443A0AA61'
  checksumType64= 'sha256'

  validExitCodes= @(0, 3010, 1641)
  silentArgs   = '/s /v"/qn"'
}

Install-ChocolateyInstallPackage @packageArgs










    








